package com.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JournalEntry {
    private String title;
    private Date created;
    private String summary;
    private final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    public JournalEntry(String title, String summary, String date) throws
            ParseException{
        this.title = title;
        this.summary = summary;
        this.created = format.parse(date);
    }
    JournalEntry(){}
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public Date getCreated() {
        return created;
    }
    public void setCreated(String date) throws ParseException {
        Long _date = null;
        try{
            _date = Long.parseLong(date);
            this.created = new Date(_date);
            return;
        }catch(Exception ignored){}
        this.created = format.parse(date);
    }
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
    public String toString(){
        return "* JournalEntry(" + "Title: " +
                title +
                ",Summary: " +
                summary +
                ",Created: " +
                format.format(created) +
                ")";
    }
}
